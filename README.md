# WhatsNew

Store release notes as a collection of .md files which are aggregated into one simple HTML page at `/whats-new`

The app can also specify the maximum version to show, eg `/whats-new/2019-2`. This is useful when production users should see less than beta testers.

## Installation

Run `composer require lasmit/whatsnew`.

## Usage

In your main laravel app create the folder `individual-release-notes` in the main `resources` folder.

You can then add release notes with the following file format:

`version-number_release-date.md`

eg

`2019-1_2019-01-13.md`

## Known Issues

- If using headings in the file, the first heading should be a h1 (ie single #).