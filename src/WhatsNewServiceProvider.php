<?php

namespace Lasmit\WhatsNew;

use Illuminate\Support\ServiceProvider;

class WhatsNewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Lasmit\WhatsNew\Http\Controllers\WhatsNewController');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');   
        $this->loadViewsFrom(__DIR__.'/resources/views', 'whatsnew');             
    }
}
