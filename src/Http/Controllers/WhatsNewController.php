<?php

namespace Lasmit\WhatsNew\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use \Storage;
use GrahamCampbell\Markdown\Facades\Markdown;


class WhatsNewController extends Controller
{
    /// Compare two release objects based on their year and version number and return which is higher
    private static function releasesCompare($a, $b) {
        if ($b->year > $a->year) {
            return 2;
        }

        if ($b->year == $a->year && $b->version > $a->version) {
            return 1;
        }

        if ($b->year == $a->year && $b->version == $a->version) {
            return 0;
        }

        return -1;
    }
    
    public function index($maxVersion = null, Request $request) {
        
        if ($maxVersion == null) {
            $maxVersion = '2200-1';
        }

        $colors = [];
        $colors['text'] = "#5D5C5C";
        $colors['bar'] = "#EEE";
        $colors['background'] = "#FFF";
        
        $colorsToCheck = ['background', 'text', 'bar'];
        foreach ($colorsToCheck as $color) {
            if ($request->input($color)) {
                $colors[$color] = "#" . $request->input($color);
            }
        }
        
        $parts = explode("-", $maxVersion);
        $maxYear = (int) $parts[0];
        $maxVersion = (int) $parts[1];
    
        $releaseNotesFilesPath = resource_path('individual-release-notes');
        $releaseNotesFiles = scandir($releaseNotesFilesPath);

        $matchingFiles = preg_grep('/^.*.md/', $releaseNotesFiles);
        rsort($matchingFiles);

        $releases = array();

        foreach ($matchingFiles as $file) {
            $release = explode("_", $file);
            $release['versionNumber'] = $release[0];
            $release['date'] = explode(".", $release[1])[0];
            
            $parts = explode("-", $release['versionNumber']);
            $release['year'] = (int) $parts[0];
            $release['version'] = (int) $parts[1];
        
            $release['versionNumber'] = str_replace("-", ".", $release['versionNumber']);
            
            if ($release['year'] > $maxYear) {
                continue;
            }

            if ($release['year'] == $maxYear && $release['version'] > $maxVersion) {
                continue;
            }

            $release = (object) $release;
            $fileContents = file_get_contents($releaseNotesFilesPath . '/' . $file);
            $release->notes = Markdown::convertToHtml($fileContents); 
            
            $releases[] = $release;
        }

        uasort($releases, 'static::releasesCompare');
        
        return view('whatsnew::whats-new', [
            'releases' => $releases,
            'colors' => $colors
        ]); 
    }
}