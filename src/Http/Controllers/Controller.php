<?php

namespace Lasmit\WhatsNew\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

     /**
     * Response status code
     * @var integer
     */ 
    protected $statusCode = 200;

    /**
     * Used for logging
     * @var string
     */
    protected $sessionId = "NOT SET";

    public function setStatusCode($statusCode) {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function respond($data, $headers = []) {
        Log::debug("Sending response:", ['status' => $this->statusCode, 'session' => $this->sessionId, 'response' => $data]);

        return Response::json($data, $this->statusCode, $headers);
    }

    public function respondWithDataUpdatedSuccessfully() {
        return $this->respond(["success"=>true]);
    }

    public function respondWithError($message) {
        return $this->respond([
            'success' => false, 
            'message' => $message            
        ]);
    }

    public function respondWithInvalidInput($message = "Invalid input") {
        return $this->setStatusCode(400)->respondWithError($message);
    }

    public function respondWithNotFound() {
        return $this->setStatusCode(404)->respondWithError("Not found");
    }
}
