<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>What's New</title>

        <style>
            body { 
                    font-family: sans-serif;
                    color: {{ $colors['text'] }};
                    background-color: {{ $colors['bar'] }};
                    margin: 0;
                    padding: 0;
            }
            header { padding: 0 1em; }
            header h1 { float: left }
            header h3 { float: right }
            section { 
                    clear: both;
                    background-color: {{ $colors['background'] }}; 
                    margin: 0;
                    padding: 1em;
            }
            li { margin-top: 0.5em }
            
        </style>

    </head>
    <body>

        @foreach ($releases as $release)

        <header>
            <h1>{{ $release->versionNumber }}</h1>
            <h3>{{ $release->date }}</h3>
        </header>
        <section>
            {!! $release->notes !!}
        </section>
        
        @endforeach

        <section>
        {{-- Give space for upgrade button --}}
        <br/>
        <br/>
        </section>
    </body>
</html>
